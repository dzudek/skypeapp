/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#pragma once

#include <QThread>
#include <QVector>
#include <QLocalServer>
#include <QLocalSocket>

#ifndef LOCAL_SERVER_NAME
#define LOCAL_SERVER_NAME "localhost"
#endif

class LocalServer : public QThread
{
	Q_OBJECT

public:
	LocalServer();
	~LocalServer();

	void shut();

protected:
	void run();
	void exec();

signals:
	void dataReceived(QString data);
	void privateDataReceived(QString data);
	void showUp();

private slots:
	void onNewConnection();
	void onPrivateDataReceived(QString data);

private:
	QLocalServer* _server;
	QVector<QLocalSocket*> _clients;

	void onCMD(QString data);

};
