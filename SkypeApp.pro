#-------------------------------------------------
#
# Project created by QtCreator 2013-04-22T10:50:16
#
#-------------------------------------------------

QT       += core gui webkit network
!win32 {
QT       += 	dbus
}

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SkypeApp
TEMPLATE = app


SOURCES += main.cpp\
        mainapplication.cpp \
    SkypeIPCProtocol.cpp \
    SkypeManager.cpp \
    GCalendarLoginDialog.cpp \
    GoogleCalendarApi.cpp \
    GoogleCalendarAccess.cpp

HEADERS  += mainapplication.h \
    SkypeProtocol.h \
    SkypeIPCProtocol.h \
    SkypeIPCStream.h \
    SkypeException.h \
    SkypeManager.h \
    GCalendarLoginDialog.h \
    GoogleCalendarApi.h \
    GoogleCalendarAccess.h
	
	
!win32 {
    SOURCES += SkypeDBUSStream.cpp

    HEADERS += SkypeDBUSStream.h

    INCLUDEPATH += /usr/include/qjson/
    LIBS += /usr/lib/x86_64-linux-gnu/libqjson.so
}
win32 {
SOURCES += SkypeCOMProtocol.cpp

HEADERS += SkypeCOMProtocol.h
}

FORMS    += mainapplication.ui \
    logindialog.ui

RESOURCES += \
    images.qrc
