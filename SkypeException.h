/*
*	Authors:
*	Pawel Drzycimski <pdrzycimski@gmail.com>
*	Karol Judek <jkarol@interia.eu>
*/

#ifndef SKYPEEXCEPTION_H
#define SKYPEEXCEPTION_H

#include <QString>

class SkypeException
{
public:
	SkypeException(const QString &msg="SkypeException"):msg(msg){}
	QString msg;
};

#endif // SKYPEEXCEPTION_H
