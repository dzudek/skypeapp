/*
*	Authors:
*	Pawel Drzycimski <pdrzycimski@gmail.com>
*/

#ifndef SKYPEIPCSTREAM_H
#define SKYPEIPCSTREAM_H

#include <QString>

class SkypeIPCStream
{
public:

    virtual void Connect() = 0;
    virtual void Close() = 0;
    virtual void Write(const QString &request) = 0;
    virtual QString Read() = 0;

};

#endif // SKYPEIPCSTREAM_H
