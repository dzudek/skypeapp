/*
*	Authors:
*	Pawel Drzycimski <pdrzycimski@gmail.com>
*	Karol Judek <jkarol@interia.eu>
*/

#include "SkypeIPCProtocol.h"

SkypeIPCProtocol::SkypeIPCProtocol(SkypeIPCStream *stream) : _stream(stream)
{
    _connected = false;
}

SkypeIPCProtocol::~SkypeIPCProtocol(void)
{
}

void SkypeIPCProtocol::Connect()
{
    try{
        _stream->Connect();
    }
    catch(SkypeException &e)
    {
        _connected = false;
        throw SkypeException(e);
        return;
    }

    _connected = true;
}

void SkypeIPCProtocol::Disconnect()
{
    if(_stream)
    {
        _stream->Close();
    }

    _connected = false;
}

void SkypeIPCProtocol::SetName(const QString &name)
{
	QString request = "NAME "+name;
	_stream->Write(request);
	QString resp = _stream->Read();
	if(resp != "OK"){
		qCritical() << "Wrong response '" << resp << "'";
		throw SkypeException("Wrong response from Skype");
	}
}


void SkypeIPCProtocol::SetProtocol(SkypeProtocols protocol)
{
	QString strprot = GetProtocolStr(protocol);
	_stream->Write(strprot);
	QString resp = _stream->Read();
	if(resp != strprot){
		qCritical() << "Wrong response '" << resp << "'";
		throw SkypeException("Wrong response from Skype");
	}
}

QString SkypeIPCProtocol::GetProtocolStr(SkypeProtocols protocol)
{
	if(protocol == PROTOCOL_1){
		return "PROTOCOL 1";
	}

	qCritical() << "Error parsing protocol version";
	throw SkypeException("Error parsing protocol version");
}

void SkypeIPCProtocol::SetStatusTo(Status st)
{
	if(!IsConnected())
	{
		qWarning() << "Not connected to Skype";
		throw SkypeException("Not connected.");
	}

	QString status = GetStatusCode(st);
	QString request = "SET USERSTATUS " + status;

	_stream->Write(request);
	QString resp = _stream->Read();

	if(!resp.contains(status)){
		qCritical() << "Response '" + resp + "' doesn't contain passed status '" + status +"'";
		throw SkypeException("Wrong response from Skype");
	}

}

QString SkypeIPCProtocol::GetStatusCode(Status st)
{
	//SET USERSTATUS ONLINE
	if(st == ST_ONLINE){
		return "ONLINE";
	}
	else if(st == ST_OFFLINE){
		return "OFFLINE";
	}

	qWarning() << "Error parsing user status" << st;
	throw SkypeException("Error parsing user status");
}

SkypeProtocol::Status SkypeIPCProtocol::GetStatus()
{
	QString request = "GET USERSTATUS";

	_stream->Write(request);
	QString resp = _stream->Read();

	resp = resp.toUpper().simplified();

	if(!resp.startsWith("USERSTATUS")){
		qCritical() << "Error in response '" + resp + "'";
		throw SkypeException("Wrong response from Skype");
	}

	resp = resp.remove("USERSTATUS");

	if(resp.contains("ONLINE"))
	{
		return ST_ONLINE;
	}
	else if(resp.contains("OFFLINE"))
	{
		return ST_OFFLINE;
	}
	else if(resp.contains("SKYPEME"))
	{
		return ST_SKYPEME;
	}
	else if(resp.contains("AWAY"))
	{
		return ST_AFK;
	}
	else if(resp.contains("DND"))
	{
		return ST_DO_NOT_DISTURB;
	}
	else if(resp.contains("INVISIBLE"))
	{
		return ST_INVISIBLE;
	}
	else if(resp.contains("LOGGEDOUT"))
	{
		return ST_LOGGEDOUT;
	}
	else if(resp.contains("UNKNOWN"))
	{
		return ST_UNKNOWN;
	}
	else if(resp.contains("NA"))
	{
		return ST_NA;
	}
	else
	{
		//This shouldn't occur
		qWarning() << "Error in response '" + resp + "'";
		throw SkypeException("Unknown status.");
	}
	
	qCritical() << "This line shouldn't be called ever!";

}
