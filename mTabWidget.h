/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#ifndef MTABWIDGET_H
#define MTABWIDGET_H

#include <QTabWidget>
#include <QTabBar>

class mTabWidget : public QTabWidget
{
	Q_OBJECT

public:
	mTabWidget(QWidget *parent);
	~mTabWidget();

public slots:
	inline void showTabBar(bool b) { tabBar()->setVisible(b); };
	inline void hideTabBar(bool b) { showTabBar(!b); };

private:
	
};

#endif // MTABWIDGET_H
