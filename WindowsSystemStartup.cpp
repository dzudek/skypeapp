/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#include "WindowsSystemStartup.h"

#include <QCoreApplication>
#include <QSettings>

#include <QDebug>

bool WindowsSystemStartup::isSet()
{
	QSettings settings("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Run", QSettings::NativeFormat);

	return settings.contains(QCoreApplication::applicationName());;
}

void WindowsSystemStartup::toggle(bool set /* = false */)
{
	qDebug() << "Changing Windows Registry startup " << set;

	QSettings settings("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Run", QSettings::NativeFormat);

	if(set)
	{
		settings.setValue(QCoreApplication::applicationName(), QCoreApplication::applicationFilePath().replace('/', '\\'));
	}
	else
	{
		settings.remove(QCoreApplication::applicationName());
	}
}
