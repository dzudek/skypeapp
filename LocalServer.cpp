/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#include "LocalServer.h"

#include <QFile>
#include <QStringList>

LocalServer::LocalServer()
{

}

LocalServer::~LocalServer()
{
	_server->close();
	for(int i = 0; i < _clients.size(); ++i)
	{
		_clients[i]->close();
	}
}

#pragma region "LocalServer protected methods (inherited from QThread)"
void LocalServer::run()
{
	_server = new QLocalServer();

	QObject::connect(_server, SIGNAL(newConnection()), this, SLOT(onNewConnection()));
	QObject::connect(this, SIGNAL(privateDataReceived(QString)), this, SLOT(onPrivateDataReceived(QString)));

#ifdef Q_OS_UNIX
	/* From QT docs:
	* "Note: On Unix if the server crashes without closing listen will fail
	* with AddressInUseError. To create a new server the file should be
	* removed. On Windows two local servers can listen to the same pipe at the
	* same time, but any connections will go to one of the server."
	*/
	QFile address(QString("/tmp/" LOCAL_SERVER_NAME));
	if(address.exists()){
		address.remove();
	}
#endif

	QString serverName = QString(LOCAL_SERVER_NAME);
	_server->listen(serverName);
	while(_server->isListening() == false){
		_server->listen(serverName);
		msleep(100);
	}
	exec();
}

void LocalServer::exec()
{
	while(_server->isListening())
	{
		msleep(100);
		_server->waitForNewConnection(100);
		for(int i = 0; i < _clients.size(); ++i)
		{
			if(_clients[i]->waitForReadyRead(100)){
				QByteArray data = _clients[i]->readAll();
				emit privateDataReceived(data);
			}
		}
	}
}
#pragma endregion

#pragma region "LocalServer private slots"
void LocalServer::onNewConnection()
{
	_clients.push_front(_server->nextPendingConnection());
}

void LocalServer::onPrivateDataReceived(QString data)
{
	if(data.contains("CMD:", Qt::CaseInsensitive)){
		onCMD(data);
	} else {
		emit dataReceived(data);
	}
}
#pragma endregion

#pragma region "LocalServer private methods"
void LocalServer::onCMD(QString data)
{
	//  Trim the leading part from the command
	data.replace(0, 4, "");

	QStringList commands;
	commands << "showUp";

	switch(commands.indexOf(data))
	{
	case 0:
		emit showUp();
	}
}
#pragma endregion
