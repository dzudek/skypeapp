NICE TO HAVE:
- changing Skype status at specified time
- advanced filtering rules for Google Calendar's events to Skype status
- spelling check

DONE:
- changing Available/Busy Skype status while it's taking is changing the appropriate status immediately
- connecting to Skype is not blocking application any more