/*
*	Authors:
*	Pawel Drzycimski <pdrzycimski@gmail.com>
*/

#include "SkypeDBUSStream.h"
#include "SkypeException.h"

#include <QtDBus/QtDBus>
#include <QtDBus/QDBusConnection>

const QString SkypeDBUSStream::_SERVICE = "com.Skype.API";
const QString SkypeDBUSStream::_PATH_SKYPE = "/com/Skype";
const QString SkypeDBUSStream::_PATH_CLIENT = "/com/Skype/Client";

SkypeDBUSStream::SkypeDBUSStream(QObject *owner)
    : SkypeIPCStream(), _owner(owner)
{
    _connectCalled = false;
    _writeCalled = false;
}
//======================================
SkypeDBUSStream::~SkypeDBUSStream()
{
    if(_conn)
    {
        delete _conn;
        _conn = 0;
    }
}
//=====================================
void SkypeDBUSStream::Connect()
{
    _lastReply = "";
    /*QString skypeServiceName("com.Skype.API");
    QDBusInterface iface(skypeServiceName, "/com/Skype", "com.Skype.API", QDBusConnection::sessionBus());*/
    /* if (iface.isValid()) {
        // tell Skype that an application called qomodoro wants to communicate with it
        QDBusReply<QString> reply = iface.call("Invoke", "NAME qomodoro");

        if (reply.isValid()) {
            // specify the protocol
            reply = iface.call("Invoke", "PROTOCOL 1");

            if (reply.isValid()) {
                // set status
                reply = iface.call("Invoke", QString("SET USERSTATUS %1").arg(iStatusName));
            }
        }
    }*/
    if(!QDBusConnection::sessionBus().isConnected()){
        throw SkypeException("SkypeDBUSStream::Connect(): Connection is not possible, can't reach D-BUS server.");
    }

    /*if(!QDBusConnection::sessionBus().registerObject(_PATH_CLIENT, this)){
        throw SkypeException("SkypeDBUSStream::Connect(): Can't register object.");
    }*/


    _conn = new QDBusInterface(_SERVICE,_PATH_SKYPE,_SERVICE,QDBusConnection::sessionBus());

    if(!_conn)
    {
        throw SkypeException("SkypeDBUSStream::Connect");
    }

    if(!_conn->isValid())
    {
        delete _conn;
        _conn = 0;
        throw SkypeException("SkypeDBUSStream::Connect(): Can't create a valid interface");
    }
    _connectCalled = true;
    /*QDBusReply<QString> reply = iface.call("Invoke", "NAME " + name);

    if(!reply.isValid()){
        throw SkypeException("SkypeDBUSStream::Connect(): Invalid response for 'NAME' request - '" + reply.error().message() + "'");
    }*/


}
//=====================================
void SkypeDBUSStream::Close()
{
    _lastReply = "";
    if(_conn)
    {
        delete _conn;
        _conn = 0;
    }
    _connectCalled = false;
}
//=====================================
void SkypeDBUSStream::Write(const QString &request)
{
    _lastReply = "";
    try{
        CheckIfConnected();
    }
    catch(SkypeException &e)
    {
        if(_connectCalled){
            Connect(); // try to re-connect
        }
        else{
            throw e;
        }
    }

    QDBusReply<QString> reply = _conn->call("Invoke",request);
    _lastReply = reply.value();
    _writeCalled = true;
}
//=====================================
QString SkypeDBUSStream::Read()
{
    if(!_writeCalled){
        return "";
    }

    _writeCalled = false;
    return _lastReply;
}
//======================================
void SkypeDBUSStream::CheckIfConnected()
{
    if(!_conn || !_conn->isValid()){
        throw SkypeException("SkypeDBUSStream: Not connected!");
    }
}


















