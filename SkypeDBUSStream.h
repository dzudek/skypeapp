/*
*	Authors:
*	Pawel Drzycimski <pdrzycimski@gmail.com>
*/

#ifndef SKYPEDBUSPROTOCOL_H
#define SKYPEDBUSPROTOCOL_H

#include "SkypeIPCStream.h"
#include <QObject>

class QDBusInterface;

class SkypeDBUSStream : public SkypeIPCStream
{
public:
    SkypeDBUSStream(QObject *_owner = 0);
    virtual ~SkypeDBUSStream();

    virtual void Connect();
    virtual void Close();
    virtual void Write(const QString &request);
    virtual QString Read();

private:
    void CheckIfConnected();

private:
    QObject *_owner;

    QDBusInterface *_conn;

    static const QString _SERVICE;
    static const QString _PATH_SKYPE;
    static const QString _PATH_CLIENT;

    bool _connectCalled;
    bool _writeCalled;
    QString _lastReply;
};

#endif // SKYPEDBUSPROTOCOL_H
