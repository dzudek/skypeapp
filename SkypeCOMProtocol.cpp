﻿/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#include "SkypeCOMProtocol.h"

#include <QMessageBox>
#include <QDebug>

SkypeCOMProtocol::SkypeCOMProtocol(void)
{
	HRESULT hr = _pSkype.CreateInstance(__uuidof(SKYPE4COMLib::Skype));

	if(hr != S_OK)
	{
		qCritical() << "Unable to create COM instance";
		throw SkypeException("Unable to create COM instance");
	}
}

SkypeCOMProtocol::~SkypeCOMProtocol(void)
{
	_pSkype->Release();
}

void SkypeCOMProtocol::Connect()
{
	if(_pSkype->GetClient()->IsRunning == VARIANT_FALSE)
	{
		qWarning() << "Skype application is not running";
		//_pSkype->GetClient()->Start(VARIANT_TRUE, VARIANT_FALSE);
		throw SkypeException("Skype application is not running.");
	}
	
	_pSkype->Attach(8, VARIANT_FALSE);

	SKYPE4COMLib::TAttachmentStatus attachStatus = _pSkype->GetAttachmentStatus();

	while(attachStatus != SKYPE4COMLib::apiAttachSuccess)
	{
		if(attachStatus == SKYPE4COMLib::apiAttachRefused)
		{
			qWarning() << "Connection refused by Skype";
			throw SkypeException("Connection refused by Skype.");
		}
		
		if(attachStatus == SKYPE4COMLib::apiAttachNotAvailable)
		{
			qWarning() << "Connection to Skype not available";
			break;
		}

		Sleep(500);
		attachStatus = _pSkype->GetAttachmentStatus();
	}
}

void SkypeCOMProtocol::Disconnect()
{
	qCritical() << "Trying to disconnect from Skype on Windows";
	//Disconnecting is not implemented in SKYPE4COM.dll library right now.
	//See: https://jira.skype.com/browse/SPA-637‎
} 

bool SkypeCOMProtocol::IsConnected()
{
	SKYPE4COMLib::TAttachmentStatus attachStatus = _pSkype->GetAttachmentStatus();

	return (attachStatus == SKYPE4COMLib::apiAttachSuccess);
}

void SkypeCOMProtocol::SetStatusTo(Status st)
{
	if(!IsConnected())
	{
		throw SkypeException("Not connected.");
	}

	/*
	SKYPE4COMLib::TUserStatus
	{
	cusUnknown = -1,
	cusOffline = 0,
	cusOnline = 1,
	cusAway = 2,
	cusNotAvailable = 3,
	cusDoNotDisturb = 4,
	cusInvisible = 5,
	cusLoggedOut = 6,
	cusSkypeMe = 7
	};
	*/
	SKYPE4COMLib::TUserStatus newStatus = SKYPE4COMLib::cusUnknown; //-1

	switch (st)
	{
	case SkypeProtocol::ST_ONLINE:
		newStatus = SKYPE4COMLib::cusOnline; //1
		break;
	case SkypeProtocol::ST_OFFLINE:
		newStatus = SKYPE4COMLib::cusOffline; //0
		break;
	case SkypeProtocol::ST_DO_NOT_DISTURB:
		newStatus = SKYPE4COMLib::cusDoNotDisturb; //4
		break;
	case SkypeProtocol::ST_AFK:
		newStatus = SKYPE4COMLib::cusAway; //2
		break;
	case SkypeProtocol::ST_INVISIBLE:
		newStatus = SKYPE4COMLib::cusInvisible; //5
		break;
	default:
		qWarning() << "Unknown status";
		throw SkypeException("Unknown status.");
		break;
	}

	HRESULT hr = _pSkype->ChangeUserStatus(newStatus);

	if(hr != S_OK) 
	{
		qWarning() << "Cannot change Skype status";
		throw SkypeException("Cannot change Skype status.");
	}
}

SkypeProtocol::Status SkypeCOMProtocol::GetStatus() 
{
	SKYPE4COMLib::TUserStatus oldStatus = _pSkype->GetCurrentUserStatus();

	switch (oldStatus)
	{
	case SKYPE4COMLib::cusOffline:
		return SkypeProtocol::ST_OFFLINE;
		break;
	case SKYPE4COMLib::cusOnline:
		return SkypeProtocol::ST_ONLINE;
		break;
	case SKYPE4COMLib::cusAway:
		return SkypeProtocol::ST_AFK;
		break;
	case SKYPE4COMLib::cusDoNotDisturb:
		return SkypeProtocol::ST_DO_NOT_DISTURB;
		break;
	case SKYPE4COMLib::cusSkypeMe:
		return SkypeProtocol::ST_SKYPEME;
		break;
	case SKYPE4COMLib::cusInvisible:
		return SkypeProtocol::ST_INVISIBLE;
		break;
	case SKYPE4COMLib::cusLoggedOut:
		return SkypeProtocol::ST_LOGGEDOUT;
		break;
	case SKYPE4COMLib::cusNotAvailable:
		return SkypeProtocol::ST_NA;
		break;
	case SKYPE4COMLib::cusUnknown:
		return SkypeProtocol::ST_UNKNOWN;
		break;

	default: //This shouldn't occur
		qWarning() << "Unknown status" << oldStatus;
		throw SkypeException("Unknown status.");
		break;
	}

	qCritical() << "This line shouldn't be called ever!";

}