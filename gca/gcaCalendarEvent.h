/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#pragma once

#include <QDateTime>
#include <QUrl>
#include <QString>
#include <QJsonObject>
#include "gcaUser.h"

class gcaCalendarEvent
{
public:
	QString etag;
	QString id;
	QString status;
	QUrl htmlLink;
	QString summary;
	QString description;
	QString location;
	gcaUser* creator;
	gcaUser* organizer;
	QDateTime start;
	QDateTime end;
	bool transparent;
	QList<gcaUser*> *attendees;
	
	gcaCalendarEvent(const QJsonObject jsonObject);
	~gcaCalendarEvent();
};

