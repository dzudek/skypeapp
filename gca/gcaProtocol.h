/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#pragma once

#include <QObject>
#include <QNetworkReply>

#include "gcaCalendarList.h"
#include "gcaCalendarEventList.h"
#include "gcaFreeBusy.h"

class QNetworkAccessManager;

class gcaLoginDialog;

class O2Gca;
class gcaRequestor;

class gcaProtocol : public QObject
{
	Q_OBJECT

public:
	O2Gca *o2;

	gcaProtocol(QObject *parent);
	~gcaProtocol();
	
	void calendarListRequest(QString pageToken = "");
	void calendarEventsRequest(QString lastCalendarId = "", QString pageToken = "");
	void calendarEventsRequest(QDateTime timeMin, QDateTime timeMax, QString lastCalendarId = "", QString pageToken = "");

	void freeBusyRequest();
	void freeBusyRequest(QDateTime timeMin, QDateTime timeMax);

signals:
	//void loginDone();
	void calendarListReady();
	void freeBusyReady();
	void calendarEventsReady();
	
	void gcaError(const QString &desc);

private slots:
	void onOpenBrowser(QUrl url);
	void onCloseBrowser();

	void onRequestFinished(int id, QNetworkReply::NetworkError error, QByteArray data, QUrl url);

	inline void calendarListRequestSlot() { calendarListRequest(); }

private:
	enum replyType {
		UNDEFINED,
		CALENDARLISTGET,
		EVENTLISTGET,
		FREEBUSYPOST
	};

	gcaLoginDialog *_loginDialog;

	QNetworkAccessManager *_manager;
	gcaRequestor *_requestor;
	
	gcaCalendarList *_calendarList;
	gcaCalendarEventList *_calendarEventList;

	QMap<int, replyType> *_replyMap;

};

