/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#pragma once

#include <QObject>

class gcaProtocol;
class QTimer;

class gcaManager : public QObject
{
	Q_OBJECT

public:
	gcaManager(QObject *parent = 0);
	~gcaManager();
	
	bool isConnected() const;
	bool isFreeBusyReady() const;

public slots:
	void connect();
	void disconnect();

	void intervalChanged(bool force = true);

signals:
	void connectionChanged(bool connected);
	void ready();

	void gcaError(const QString &desc);

private slots:
	void onLinkedChanged();
	
	void onCalendarListReady();
	void onFreeBusyReady();
	void onCalendarEventsReady();

	void onError(const QString &desc);

	void onTimerTimeout();

private:
	gcaProtocol *_protocol;
	QTimer *_timer;
};
