/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#include "gcaRequestor.h"

gcaRequestor::gcaRequestor(QNetworkAccessManager *manager, O2 *authenticator, QObject *parent /*= 0*/)
	: O2Requestor(manager, authenticator, parent)
{
	connect(this, SIGNAL(finished(int, QNetworkReply::NetworkError, QByteArray)), this, SLOT(onFinished(int, QNetworkReply::NetworkError, QByteArray)));
}

void gcaRequestor::onFinished(int id, QNetworkReply::NetworkError error, QByteArray data)
{
	emit finished(id, error, data, url_);
}