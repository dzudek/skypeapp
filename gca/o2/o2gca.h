#pragma once

#include "o2.h"

/// Google Calendar API' dialect of OAuth 2.0
class O2Gca: public O2 {
	Q_OBJECT

public:
	explicit O2Gca(QObject *parent = 0);
};
