/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#include "gcaLoginDialog.h"

#include <QUrl>
#include <QApplication>

#include <QDebug>

gcaLoginDialog::gcaLoginDialog(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::gcaLoginDialog();

	ui->setupUi(this);

	connect(ui->webView, SIGNAL(loadStarted()), this, SLOT(onLoadStarted()));
	connect(ui->webView, SIGNAL(loadFinished(bool)), this, SLOT(onLoadFinished(bool)));

	connect(ui->webView->page()->networkAccessManager(), SIGNAL(sslErrors(QNetworkReply*, const QList<QSslError> & )), this, SLOT(handleSslErrors(QNetworkReply*, const QList<QSslError> & )));
}

gcaLoginDialog::~gcaLoginDialog()
{
	delete ui;
}

#pragma region "gcaLoginDialog public methods"
void gcaLoginDialog::setLoginUrl(const QUrl& url)
{
	qDebug() << "Setting logging url =" << url.toDisplayString();

	ui->webView->setUrl(url);
}
#pragma endregion

#pragma region "gcaLoginDialog private slots"
void gcaLoginDialog::onLoadStarted()
{
	qDebug() << "Loading started";

	QApplication::setOverrideCursor(Qt::WaitCursor);
}

void gcaLoginDialog::onLoadFinished(bool ok)
{
	qDebug() << "Loading finished" << ok;

	QApplication::restoreOverrideCursor();
}

void gcaLoginDialog::handleSslErrors(QNetworkReply* reply, const QList<QSslError> &errors)
{
	qWarning() << "SSL Error occurs";

	//Just in case...
	reply->ignoreSslErrors();
}
#pragma endregion
