/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#pragma once

#include <QString>

class QJsonObject;

class gcaCalendar
{
public:
	QString etag;
	QString id;
	QString summary;
	QString description;
	QString location;
	QString timeZone;
	QString summaryOverride;
	QString colorId;
	QString backgroundColor;
	QString foregroundColor;
	bool hidden;
	bool selected;
	QString accessRole;

	gcaCalendar(const QJsonObject jsonObject);
};

