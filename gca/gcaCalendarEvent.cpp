/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#include "gcaCalendarEvent.h"

#include <QString>
#include <QJsonObject>
#include <QJsonArray>
#include <QUrl>

gcaCalendarEvent::gcaCalendarEvent(const QJsonObject jsonObject)
{
	etag = jsonObject["etag"].toString();
	id = jsonObject["id"].toString();
	status = jsonObject["status"].toString();
	htmlLink = QUrl(jsonObject["htmlLink"].toString());
	summary = jsonObject["summary"].toString();
	description = jsonObject["description"].toString();
	location = jsonObject["location"].toString();
	creator = new gcaUser(jsonObject["creator"].toObject());
	organizer = new gcaUser(jsonObject["organizer"].toObject());

	start = QDateTime::fromString(jsonObject["start"].toObject()["dateTime"].toString(), Qt::ISODate);
	end = QDateTime::fromString(jsonObject["start"].toObject()["dateTime"].toString(), Qt::ISODate);

	transparent = (jsonObject["transparent"].toString() == "transparent");

	//TODO
	attendees = new QList<gcaUser*>();

	if(jsonObject["attendees"].isArray()) {
		QJsonArray ar = jsonObject["attendees"].toArray();

		for(int i = 0; i < ar.size(); ++i)
			attendees->push_back(new gcaUser(ar[i].toObject()));
	}

}

gcaCalendarEvent::~gcaCalendarEvent()
{
	delete creator;
	delete organizer;

	delete attendees;

	//TODO deleting attendees
}
