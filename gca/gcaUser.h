/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#pragma once

#include <Qstring>
class QJsonObject;

class gcaUser
{
public:
	QString id;
	QString email;
	QString displayName;
	bool self;

	gcaUser(const QJsonObject jsonObject);
};

