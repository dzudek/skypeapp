/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#pragma once

#include "o2\o2requestor.h"

class gcaRequestor : public O2Requestor
{
	Q_OBJECT
public:
	gcaRequestor(QNetworkAccessManager *manager, O2 *authenticator, QObject *parent = 0);

signals:
	void finished(int id, QNetworkReply::NetworkError error, QByteArray data, QUrl url);

private slots:
	void onFinished(int id, QNetworkReply::NetworkError error, QByteArray data);


};

