#pragma once

#include <QDateTime>;

struct gcaEvent
{
	QDateTime start;
	QDateTime end;
};
