/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#include "gcaUser.h"

#include <QJsonObject>

gcaUser::gcaUser(const QJsonObject jsonObject)
{
	id = jsonObject["id"].toString();
	email = jsonObject["email"].toString();
	displayName = jsonObject["displayName"].toString();
	self = jsonObject["self"].toBool();
}
