/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#pragma once

#include <QString>
#include <QList>

#include "gcaCalendar.h"

static const char *gcaCalendarListEndpoint = "https://www.googleapis.com/calendar/v3/users/me/calendarList";


class gcaCalendarList
{
public:
	QString etag;
	QString nextPageToken;
	QList<gcaCalendar> *calendars;

	gcaCalendarList();
	gcaCalendarList(const QByteArray jsonData);
	~gcaCalendarList();

	void parseJson(const QByteArray jsonData);

	void clear();

	QString nextCalendarId(QString calendarId);
};
