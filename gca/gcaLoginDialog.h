/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#pragma once

#include <QWidget>
#include <QNetworkReply>
#include <QSslError>

#include "ui_gcaLoginDialog.h"

class gcaLoginDialog : public QWidget
{
	Q_OBJECT

public:
	gcaLoginDialog(QWidget *parent = 0);
	~gcaLoginDialog();
	
	void setLoginUrl(const QUrl& url);

private:
	Ui::gcaLoginDialog *ui;

private slots:
	void onLoadStarted();
	void onLoadFinished(bool);

	void handleSslErrors(QNetworkReply* reply, const QList<QSslError> &errors);
};
