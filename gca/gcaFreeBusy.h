/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#pragma once

#include <QString>
#include <QDateTime>
#include <QMutex>

static const char *gcaFreeBusyEndpoint = "https://www.googleapis.com/calendar/v3/freeBusy";

typedef QPair<QDateTime,QDateTime> gcaEvent;

class gcaFreeBusy
{
public:
	QDateTime timeMin;
	QDateTime timeMax;

	void parseJson(const QByteArray jsonData);
	bool isBusy(const QDateTime date = QDateTime::currentDateTimeUtc());
	inline bool isReady() const { return !_events->empty(); }

private:
	QList<gcaEvent> *_events;

	void addEvent(const gcaEvent &evnt);

//singleton impl
public:
	static gcaFreeBusy* getInstance()
	{
		static QMutex mutex;

		if(!_instance)
		{
			mutex.lock();

			if(!_instance)
			{
				_instance = new gcaFreeBusy();
			}

			mutex.unlock();
		}

		return _instance;
	}

	static void dropInstance()
	{
		static QMutex mutex;

		mutex.lock();

		delete _instance;
		_instance = 0;

		mutex.unlock();
	}


private:
	static gcaFreeBusy *_instance;
	
	gcaFreeBusy();
	~gcaFreeBusy();

	gcaFreeBusy(const gcaFreeBusy &);
	gcaFreeBusy& operator=(const gcaFreeBusy &);

};
