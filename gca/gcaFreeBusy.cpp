/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#include "gcaFreeBusy.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include <QDebug>

gcaFreeBusy *gcaFreeBusy::_instance = 0;

gcaFreeBusy::gcaFreeBusy(void)
{
	_events = new QList<gcaEvent>;
}

gcaFreeBusy::~gcaFreeBusy(void)
{
	delete _events;
}

void gcaFreeBusy::parseJson(const QByteArray jsonData)
{
	QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonData);

	if (jsonDoc.isNull())
	{
		throw QString("gcaFreeBusy::parseJson: Error parsing json response.");
		return;
	}

	QJsonObject parseData = jsonDoc.object();
	
	timeMin = QDateTime::fromString(parseData["timeMin"].toString(), Qt::ISODate);
	timeMax = QDateTime::fromString(parseData["timeMax"].toString(), Qt::ISODate);

	if(!parseData["calendars"].isObject())
	{
		throw QString("gcaFreeBusy::parseJson: Missing calendars object in response.");
		return;
	}

	const QJsonObject obj = parseData["calendars"].toObject();

	//FIXME Seems to be a little bit too complicated - cleanup needed
	for(QJsonObject::const_iterator it = obj.constBegin(); it != obj.constEnd(); ++it) 
	{
		if(it.value().isObject()) // calendar element
		{
			QJsonValue busy = it.value().toObject()["busy"]; //busy array inside calendar element

			if(busy.isArray())
			{
				QJsonArray evnts = busy.toArray();

				for(QJsonArray::const_iterator jt = evnts.constBegin(); jt != evnts.constEnd(); ++jt)
				{
					QJsonObject evnt = jt[0].toObject(); // event inside busy array inside calendar element

					addEvent(gcaEvent(
						QDateTime::fromString(evnt.find("start").value().toString(),  Qt::ISODate), 
						QDateTime::fromString(evnt.find("end").value().toString(),  Qt::ISODate)
						));

				}
			}
		}
	}

	// Sorting list for easier checking the actual free/busy status
	qSort(_events->begin(), _events->end());
	
}

bool gcaFreeBusy::isBusy(const QDateTime date /*= QDateTime::currentDateTimeUtc()*/)
{
	if(date < timeMin || date > timeMax)
	{
		throw new QString("gcaFreeBusy::isBusy: Date out of range");
		return false;
	}

	QMutableListIterator<gcaEvent> it(*_events);
	while(it.hasNext())
	{
		gcaEvent e = it.next();

		if(e.second < date) //event ended already...
		{
			it.remove(); //...so delete it
			continue;
		}

		if(e.first > date)
		{
			//assuming that list is sorted by start date and found event in the future, there is no point to searching further
			return false;
		}
		
		if(e.first <= date && e.second >= date) //this if is probably useless
		{
			return true; //bingo!
		}
	}

	return false;
}

void gcaFreeBusy::addEvent(const gcaEvent &evnt)
{
	if(!_events->contains(evnt))
	{
		_events->push_back(evnt);
	}
	// Events could be merged using Allen's interval algebra but it seems to be to overpowered for this task
	// Does not expect to be many calendars (and events) per one user so putting all events into one container seems to be optimal enough
}
