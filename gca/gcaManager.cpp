/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#include "gcaManager.h"
#include "gcaProtocol.h"

#include "o2/o2gca.h"

#include <QSettings>
#include <QTimer>

#include <QDebug>

gcaManager::gcaManager(QObject *parent /* = 0 */)
	: QObject(parent)
{
	_protocol = new gcaProtocol(this);

	QObject::connect(_protocol->o2, SIGNAL(linkedChanged()), this, SLOT(onLinkedChanged()));
	QObject::connect(_protocol->o2, SIGNAL(linkingFailed()), this, SLOT(onLinkedChanged()));
	QObject::connect(_protocol->o2, SIGNAL(linkingSucceeded()), this, SLOT(onLinkedChanged()));
	
	QObject::connect(_protocol, SIGNAL(calendarListReady()), this, SLOT(onCalendarListReady()));
	QObject::connect(_protocol, SIGNAL(freeBusyReady()), this, SLOT(onFreeBusyReady()));

	QObject::connect(_protocol, SIGNAL(gcaError(const QString &)), this, SLOT(onError(const QString &)));

	_timer = new QTimer(this);
	_timer->setTimerType(Qt::VeryCoarseTimer);
	QObject::connect(_timer, SIGNAL(timeout()), this, SLOT(onTimerTimeout()));

	if(QSettings().value("google/autoconnect", false).toBool())
	{
		QTimer::singleShot(500, this, SLOT(connect()));
	}
}

gcaManager::~gcaManager()
{
	delete _timer;

	delete _protocol;
	
	//gcaFreeBusy::dropInstance();
}

#pragma region "gcaManager public functions"
bool gcaManager::isConnected() const
{ 
	return _protocol->o2->linked();
}
#pragma endregion

#pragma region "gcaManager public slots"
void gcaManager::connect()
{
	qDebug() << "Connecting to Google";

	if(!isConnected() && !_protocol->o2->refreshToken().length())
	{
		_protocol->o2->link();
	}
	else //Seems legit...
	{
		emit connectionChanged(_protocol->o2->linked());
		_protocol->calendarListRequest();
	}
}

void gcaManager::disconnect()
{
	qDebug() << "Disconnecting to Google";

	_protocol->o2->unlink();
}

void gcaManager::intervalChanged(bool force /* = true */)
{
	qDebug() << "Changed interval timer for gcaFreeBusy; force =" << force;

	if(!_timer->isActive() || force)
	{
		if(_timer->isActive())
		{
			qDebug() << "Stopping timer";
			_timer->stop();
		}

		QTime t = QSettings().value("google/interval", "01:00:00").toTime();

		int msecInterval = ((t.hour()*60 + t.minute())*60 + t.second())*1000;

		_timer->setInterval(msecInterval);
		
		qDebug() << "Starting timer";
		_timer->start();

		onTimerTimeout();
	}
}
#pragma endregion

#pragma region "gcaManager private slots"
void gcaManager::onLinkedChanged()
{
	// Linking (login) state has changed. 
	// Use o2->linked() to get the actual state
	bool linked = _protocol->o2->linked();
	qDebug() << "O2 linkin changed to" << linked;

	emit connectionChanged(linked);

	if(linked)
	{
		_protocol->calendarListRequest();
	}
}

void gcaManager::onCalendarListReady()
{
	qDebug() << "Got calendars list";

	//get free/busy or full events list depening if do a simple check or applying rules respectively
	if(QSettings().value("skype/rules", false).toBool()) {
		_protocol->calendarEventsRequest();
	}
	else {
		_protocol->freeBusyRequest();
	}
}

void gcaManager::onFreeBusyReady()
{
	qDebug() << "Got free/busy events list";

	emit ready();

	intervalChanged(false);
}

void gcaManager::onCalendarEventsReady()
{
	qDebug() << "Got calendars events";
}

void gcaManager::onError(const QString &desc)
{
	qWarning() << desc;

	emit gcaError(desc);
}

void gcaManager::onTimerTimeout()
{
	qDebug() << "Timer timeout";

	if(QSettings().value("skype/rules", false).toBool()) {
		_protocol->calendarEventsRequest();
	}
	else {
		_protocol->freeBusyRequest();
	}
}
#pragma endregion
