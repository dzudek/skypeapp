/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#include "gcaCalendar.h"

#include <QJsonObject>

gcaCalendar::gcaCalendar(const QJsonObject jsonObject)
{
	etag = jsonObject["etag"].toString();
	id = jsonObject["id"].toString();
	summary = jsonObject["summary"].toString();
	description = jsonObject["description"].toString();
	location = jsonObject["location"].toString();
	timeZone = jsonObject["timeZone"].toString();
	summaryOverride = jsonObject["summaryOverride"].toString();
	colorId = jsonObject["colorId"].toString();
	backgroundColor = jsonObject["backgroundColor"].toString();
	foregroundColor = jsonObject["foregroundColor"].toString();
	hidden = jsonObject["hidden"].toBool();
	selected = jsonObject["selected"].toBool();
	accessRole = jsonObject["accessRole"].toString();
	//Do we need all of the above data?!
}

