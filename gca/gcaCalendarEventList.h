/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#pragma once

#include <QString>

#include "gcaCalendarEvent.h"

static const char *gcaCalendarEventListEndpoint = "https://www.googleapis.com/calendar/v3/calendars/%1/events";
static const char *gcaCalendarEventListMask = "^https://www.googleapis.com/calendar/v3/calendars/(?<id>.*)/events";

class gcaCalendarEventList
{
public:
	QString etag;
	QString nextPageToken;
	QList<gcaCalendarEvent> *events;

	gcaCalendarEventList();
	gcaCalendarEventList(const QByteArray jsonData);
	~gcaCalendarEventList();

	void parseJson(const QByteArray jsonData);

	void clear();

	static QString getCalendarId(QUrl url);
};
