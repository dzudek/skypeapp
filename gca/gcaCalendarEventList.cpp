/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#include "gcaCalendarEventList.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include <QRegularExpression>
#include <QDebug>

gcaCalendarEventList::gcaCalendarEventList(void)
{
	events = new QList<gcaCalendarEvent>;
}

gcaCalendarEventList::gcaCalendarEventList(const QByteArray jsonData)
{
	events = new QList<gcaCalendarEvent>;

	parseJson(jsonData);
}

gcaCalendarEventList::~gcaCalendarEventList(void)
{
	delete events;
}

void gcaCalendarEventList::parseJson(const QByteArray jsonData)
{
	QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonData);

	if (jsonDoc.isNull())
	{
		throw QString("gcaCalendarEventList::parseJson: Error parsing json response.");
		return;
	}

	QJsonObject parseData = jsonDoc.object();
	
	etag = parseData["etag"].toString();
	nextPageToken = parseData["nextPageToken"].toString();

	if(!parseData["items"].isArray())
	{
		throw QString("gcaCalendarEventList::parseJson: Missing items array in response.");
		return;
	}

	const QJsonArray arr = parseData["items"].toArray();

	for(int i = 0; i < arr.size(); ++i)
	{
		events->push_back(gcaCalendarEvent(arr[i].toObject()));
	}
}

void gcaCalendarEventList::clear()
{
	etag = "";
	nextPageToken = "";

	events->clear();
}


QString gcaCalendarEventList::getCalendarId(QUrl url)
{
	QRegularExpression re(gcaCalendarEventListMask);

	if(!re.isValid()) {
		qCritical() << "Invalid regular expression" << gcaCalendarEventListMask;
		qDebug() << re.patternErrorOffset() << re.errorString();
		throw;
	}

	QRegularExpressionMatch match = re.match(url.toDisplayString());

	if(!match.hasMatch()) {
		qCritical() << "Cannot find calendar id in url" << url.toDisplayString();
		throw;
	}

	return match.captured("id");
}
