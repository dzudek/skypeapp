/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#include "gcaCalendarList.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include <QDebug>

gcaCalendarList::gcaCalendarList()
{
	calendars = new QList<gcaCalendar>;
}

gcaCalendarList::gcaCalendarList(const QByteArray jsonData)
{
	calendars = new QList<gcaCalendar>;

	parseJson(jsonData);
}

gcaCalendarList::~gcaCalendarList()
{
	delete calendars;
}

void gcaCalendarList::parseJson(const QByteArray jsonData)
{
	QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonData);

	if (jsonDoc.isNull())
	{
		throw QString("gcaCalendarList::parseJson: Error parsing json response.");
		return;
	}

	QJsonObject parseData = jsonDoc.object();
	
	etag = parseData["etag"].toString();
	nextPageToken = parseData["nextPageToken"].toString();

	if(!parseData["items"].isArray())
	{
		
		throw QString("gcaCalendarList::parseJson: Missing items array in response.");
		return;
	}

	const QJsonArray arr = parseData["items"].toArray();

	for(int i = 0; i < arr.size(); ++i)
	{
		calendars->push_back(gcaCalendar(arr[i].toObject()));
	}
}

void gcaCalendarList::clear()
{
	etag = "";
	nextPageToken = "";

	calendars->clear();
}

QString gcaCalendarList::nextCalendarId(QString calendarId)
{
	for(int it = 0; it < calendars->size(); ++it)
	{
		if(calendars->at(it).id == calendarId)
		{
			return (it < calendars->size()-1 ? calendars->at(++it).id : "");
		}
	}

	qWarning() << "Could not find calendar with id" << calendarId;
	throw QString("Could not find calendar");

	return "";
}
