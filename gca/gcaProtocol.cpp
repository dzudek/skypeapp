/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#include "gcaProtocol.h"
#include "gcaLoginDialog.h"

#include <QUrl>
#include <QUrlQuery>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QSettings>

#include "o2/o2gca.h"

#include "gcaRequestor.h"

#include "gcaManager.h"

#include <QDebug>

gcaProtocol::gcaProtocol(QObject *parent)
	: QObject(parent)
{
	_loginDialog = new gcaLoginDialog();

	o2 = new O2Gca();

	QSettings settings;

	o2->setClientId(settings.value("google/clientid", "909644294274-v06fb6v64h5v8fi4t58e6f93p05dlmku.apps.googleusercontent.com").toString());
	o2->setClientSecret(settings.value("google/clientsecret", "CBanqKrsJgVu8MqQYH8ymB2i").toString());
	
	QObject::connect(o2, SIGNAL(openBrowser(QUrl)), this, SLOT(onOpenBrowser(QUrl)));
	QObject::connect(o2, SIGNAL(closeBrowser()), this, SLOT(onCloseBrowser()));
	
	_replyMap = new QMap<int, replyType>;
	_calendarList = new gcaCalendarList();
	_calendarEventList = new gcaCalendarEventList();
	
	_manager = new QNetworkAccessManager(this);
	_requestor = new gcaRequestor(_manager, o2, this);

	QObject::connect(_requestor, SIGNAL(finished(int, QNetworkReply::NetworkError, QByteArray, QUrl)), this, SLOT(onRequestFinished(int, QNetworkReply::NetworkError, QByteArray, QUrl)));
}

gcaProtocol::~gcaProtocol()
{
	delete _requestor;
	delete _manager;

	delete o2;

	delete _loginDialog;

	delete _calendarList;

	delete _replyMap;
}

#pragma region "gcaProtocol public methods"
void gcaProtocol::calendarListRequest(QString pageToken /* = "" */)
{
	qDebug() << "Preparing request for calendar list";

	if(pageToken.isEmpty())
	{
		_calendarList->clear();
	}

	QNetworkRequest request;
	QUrl url(gcaCalendarListEndpoint);
	QUrlQuery query;

	query.addQueryItem("minAccessRole", "owner");

	if(!pageToken.isEmpty())
	{
		query.addQueryItem("pageToken", pageToken);
	}
	
	url.setQuery(query);

	qDebug() << url.toDisplayString();

	request.setUrl(url);

	qDebug() << "Calendar list request url =" << url.toDisplayString();

	int id = _requestor->get(request);

	qDebug() << "Calendar list request id =" << id;

	_replyMap->insert(id, CALENDARLISTGET);
}
void gcaProtocol::calendarEventsRequest(QString lastCalendarId /* = "" */, QString pageToken /* = "" */)
{
	qDebug() << "Getting event list for current date";
	
	calendarEventsRequest(QDateTime::currentDateTime(), QDateTime::currentDateTime().addDays(1), lastCalendarId, pageToken);
}

void gcaProtocol::calendarEventsRequest(QDateTime timeMin, QDateTime timeMax, QString lastCalendarId /* = "" */, QString pageToken /* = "" */)
{
	qDebug() << "Preparing event list request from" << timeMin.toString(Qt::ISODate) << "till" << timeMax.toString(Qt::ISODate);

	if(!_calendarList || _calendarList->calendars->isEmpty())
	{
		qCritical() << "Calendar list empty or uninitialized";
		QTimer::singleShot(2000, this, SLOT(calendarListRequestSlot()));
		emit gcaError("Cannot find calendar");
		return;
	}

	if(lastCalendarId.isEmpty())
	{
		lastCalendarId = _calendarList->calendars->at(0).id;

		if(pageToken.isEmpty()) // first attempt
		{
			_calendarEventList->clear();
		}
	}

	QNetworkRequest request;
	QUrl url(QString(gcaCalendarEventListEndpoint).arg(lastCalendarId));
	QUrlQuery query;

	if(!pageToken.isEmpty())
	{
		query.addQueryItem("pageToken", pageToken);
	}
	
	query.addQueryItem("timeMin", timeMin.toUTC().toString(Qt::ISODate));
	query.addQueryItem("timeMax", timeMax.toUTC().toString(Qt::ISODate));

	url.setQuery(query);

	qDebug() << url.toDisplayString();

	request.setUrl(url);

	qDebug() << "Event list request url =" << url.toDisplayString();

	int id = _requestor->get(request);

	qDebug() << "Event list request id =" << id;

	_replyMap->insert(id, EVENTLISTGET);
}

void gcaProtocol::freeBusyRequest()
{
	qDebug() << "Getting free/busy for current date";

	freeBusyRequest(QDateTime::currentDateTime(), QDateTime::currentDateTime().addDays(1));
}

void gcaProtocol::freeBusyRequest(QDateTime timeMin, QDateTime timeMax)
{
	qDebug() << "Preparing request for free/busy events list from" << timeMin.toString(Qt::ISODate) << "till" << timeMax.toString(Qt::ISODate);

	if(!_calendarList || _calendarList->calendars->isEmpty())
	{
		qCritical() << "Calendar list empty or uninitialized";
		QTimer::singleShot(2000, this, SLOT(calendarListRequest()));
		emit gcaError("Cannot find calendar");
		return;
	}

	QJsonObject root;
	
	root.insert("timeMin", timeMin.toUTC().toString(Qt::ISODate));
	root.insert("timeMax", timeMax.toUTC().toString(Qt::ISODate));
	
	QJsonArray items;

	QListIterator<gcaCalendar> it(*(_calendarList->calendars));
	while(it.hasNext())
	{
		QJsonObject cal;
		cal.insert("id", it.next().id);
		items.append(cal);
	}

	root.insert("items", items);

	QJsonDocument data(root);

	qDebug() << "Free/busy request data =" << data;

	QNetworkRequest request;
	QUrl url(gcaFreeBusyEndpoint);

	request.setUrl(url);
	request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
	//request.setRawHeader("X-JavaScript-User-Agent", "Google APIs Explorer");

	qDebug() << "Free/busy request url =" << url.toDisplayString();

	int id = _requestor->post(request, data.toJson());

	qDebug() << "Free/busy request id =" << id;

	_replyMap->insert(id, FREEBUSYPOST);
}
#pragma endregion

#pragma region "gcaProtocol private slots"
void gcaProtocol::onOpenBrowser(const QUrl url)
{
	qDebug() << "Opening login dialog with url =" << url.toDisplayString();
	
	// Open a web browser or a web view with the given URL.
	// The user will interact with this browser window to
	// enter login name, password, and authorize our application
	// to access the Google account

	_loginDialog->setLoginUrl(url);
	_loginDialog->show();
	_loginDialog->raise();
	_loginDialog->setFocus();
}

void gcaProtocol::onCloseBrowser()
{
	qDebug() << "Closing login dialog";

	// Close the browser window opened in openBrowser()
	_loginDialog->close();
}

void gcaProtocol::onRequestFinished(int id, QNetworkReply::NetworkError error, QByteArray data, QUrl url)
{
	qDebug() << "Finished request id =" << id << "data =" << data << "url =" << url.toDisplayString();

	if(error != QNetworkReply::NoError)
	{
		qWarning() << "Error in request id =" << id << "error" << error;

		_replyMap->remove(id);
		return;
	}

	//FIXME This is f***ing ridiculous...
	switch(_replyMap->take(id)) 
	{
	case CALENDARLISTGET:
		try
		{
			_calendarList->parseJson(data);

			if(!_calendarList->nextPageToken.isEmpty())
			{
				calendarListRequest(_calendarList->nextPageToken);
			}
			else
			{
				emit calendarListReady();
			}
		}
		catch(QString e)
		{
			qWarning() << "Calendar list parsing error" << e;
			emit gcaError(e);
		}
		catch(...)
		{	
			qWarning() << "Calendar list error";
			emit gcaError("Error parsing calendar list response.");
		}
		break;
	case EVENTLISTGET:
		try
		{
			_calendarEventList->parseJson(data);

			QString calId = gcaCalendarEventList::getCalendarId(url);

			if(!_calendarEventList->nextPageToken.isEmpty())
			{
				calendarEventsRequest(calId, _calendarEventList->nextPageToken);
				break;
			}

			calId = _calendarList->nextCalendarId(calId);

			if(!calId.isEmpty())
			{
				calendarEventsRequest(calId);
			}
			else
			{
				emit calendarEventsReady();
			}
		}
		catch(QString e)
		{
			qWarning() << "Event list parsing error" << e;
			emit gcaError(e);
		}
		catch(...)
		{	
			qWarning() << "Event list error";
			emit gcaError("Error parsing event list response.");
		}
		break;
	case FREEBUSYPOST:
		try
		{
			gcaFreeBusy::getInstance()->parseJson(data);
			emit freeBusyReady();
		}
		catch(QString e)
		{
			qWarning() << "Free/busy error" << e;
			emit gcaError(e);
		}
		catch(...)
		{
			qWarning() << "Free/busy error";
			emit gcaError("Error parsing free/busy response.");
		}

		break;

	default:
		qCritical() << "Unknown request type" << _replyMap->take(id);
		emit gcaError("Unknown request type.");
		break;
	}
}
#pragma endregion
