/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#include "mTabWidget.h"

mTabWidget::mTabWidget(QWidget *parent)
	: QTabWidget(parent)
{
	hideTabBar(true);
}

mTabWidget::~mTabWidget()
{

}
