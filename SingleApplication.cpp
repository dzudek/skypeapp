/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#include "SingleApplication.h"

SingleApplication::SingleApplication(int argc, char *argv[])
	: QApplication(argc, argv)
{
	_shouldContinue = false;

	_socket = new QLocalSocket();

	_socket->connectToServer(LOCAL_SERVER_NAME);
	if (_socket->waitForConnected(100))
	{
		//connected to other instance
		_socket->write("CMD:showUp");
		_socket->flush();
		QThread::msleep(100);
		_socket->close();
	}
	else
	{
		//no other instances
		_shouldContinue = true;

		_server = new LocalServer();
		_server->start();
		connect(_server, SIGNAL(showUp()), this, SLOT(onShowUp()));
	}
}

SingleApplication::~SingleApplication()
{
	if (_shouldContinue)
	{
		_server->terminate();
	}
}

void SingleApplication::onShowUp()
{
	emit showUp();
}
