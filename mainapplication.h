/*
*	Authors:
*	Pawel Drzycimski <pdrzycimski@gmail.com>
*	Karol Judek <jkarol@interia.eu>
*/

#ifndef MAINAPPLICATION_H
#define MAINAPPLICATION_H

#include <QMainWindow>
#include <QSystemTrayIcon>

#include "SkypeProtocol.h"
#include "ASystemStartup.h"

namespace Ui {
class MainApplication;
}

class SkypeManager;

class GoogleCalendarApi;

class QAction;
class QSystemTrayIcon;
class QMenu;
class QTimer;
class QSignalMapper;

class gcaManager;

class MainApplication : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainApplication(QWidget *parent = 0);
	~MainApplication();

public slots:
	void onShowUp();

protected:
	void changeEvent(QEvent *);
	void closeEvent(QCloseEvent *);
	void resizeEvent(QResizeEvent * evnt);

private slots:
	void OnShowHide(QSystemTrayIcon::ActivationReason reason);
	void SwitchShowHide();

	void OnSkypeConnected();
	void OnSkypeDisconnected();
	void OnSkypeError(const QString &description);
	void OnSkypeStatus(const QString &description);

	void onGoogleApiConnectionChanged(const bool &connected);
	void onGoogleApiConnected();
	void onGoogleApiDisconnected();
	void onGoogleApiReady();
	void OnGoogleApiError(const QString &desc);

	inline void onTimerTimeout();
	void updateSkypeStatus(const bool &forceUpdate = true);

	void onCheckBoxMapped(const QString &text);
	void onComboBoxMapped(const QString &text);
	void onGoogleTimeEditTimeChanged(const QTime &time);

private:
	void CreateTrayIcon();
	void DisposeActions();
	void DisposeAction(QAction *action);

	Ui::MainApplication *ui;
	SkypeManager *_skypeManager;
	gcaManager *_gcaManager;

	QAction *_hideAction;
	QAction *_trayExitAction;
	QAction *_skypeConnectAction;
	QAction *_googleCalendarConnect;

	QSystemTrayIcon *_trayIcon;
	QMenu *_trayIconMenu;

	QTimer *_timer;

	QSignalMapper *_checkBoxSignalMapper;
	QSignalMapper *_comboBoxSignalMapper;

	ASystemStartup *_startup;

	bool lastGoogleBusyStatus;
	SkypeProtocol::Status lastSkypeStatus;
	bool firstCheck;
};

#endif // MAINAPPLICATION_H
