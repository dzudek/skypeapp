/*
*	Authors:
*	Pawel Drzycimski <pdrzycimski@gmail.com>
*	Karol Judek <jkarol@interia.eu>
*/

#ifndef SKYPEMANAGER_H
#define SKYPEMANAGER_H

#include <QObject>

#include "SkypeProtocol.h"
#include "SkypeException.h"

#ifdef Q_OS_WIN
#include "SkypeCOMProtocol.h"
#else
#include "SkypeIPCStream.h"
#include "SkypeIPCProtocol.h"
#include "SkypeDBUSStream.h"
#endif

class SkypeManager : public QObject
{
	Q_OBJECT
public:
	explicit SkypeManager(QObject *parent = 0);

	bool isConnected() const;
	SkypeProtocol::Status getStatus() const;

signals:
	void Connected();
	void Disconnected();

	void ConnectionStatus(const QString &textStatus);
	void ConnectionError(const QString &description);
	void ChangeStatusError(const QString &descritpion);

public slots:
	void ConnectToSkype();
	void DisconnectFromSkype();

	void ChangeStatusTo(SkypeProtocol::Status st);

private:
	SkypeProtocol *_protocol;

};
#endif // SKYPEMANAGER_H
