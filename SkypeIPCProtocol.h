/*
*	Authors:
*	Pawel Drzycimski <pdrzycimski@gmail.com>
*	Karol Judek <jkarol@interia.eu>
*/

#pragma once
#include "SkypeProtocol.h"

class SkypeIPCStream;

class SkypeIPCProtocol :
	public SkypeProtocol
{
public:
	SkypeIPCProtocol(SkypeIPCStream *stream);
	~SkypeIPCProtocol(void);

	void Connect();
	void Disconnect();
	inline bool IsConnected() {
		return _connected;
	}

	void SetName(const QString &name);
	void SetProtocol(SkypeProtocols protocol);
	void SetStatusTo(Status st);
	Status GetStatus();

	/* Use only on opened stream! */
	inline void SetStream(SkypeIPCStream *stream)
	{
		_stream = stream;
	}

	inline SkypeIPCStream *GetStream()
	{
		return _stream;
	}

private:
	QString GetProtocolStr(SkypeProtocols protocol);
	QString GetStatusCode(Status st);

	SkypeIPCStream *_stream;

	bool _connected;

};
