/*
*	Authors:
*	Pawel Drzycimski <pdrzycimski@gmail.com>
*	Karol Judek <jkarol@interia.eu>
*/

#ifndef SKYPEPROTOCOL_H
#define SKYPEPROTOCOL_H

#include <QString>

#include "SkypeException.h"
#ifndef Q_OS_WIN
#include "SkypeIPCStream.h"
#endif

class SkypeProtocol
{
public:
	enum Status
	{
		ST_UNKNOWN = -2,
		ST_NA,
		ST_ONLINE,
		ST_AFK, // away from keyboard, zaraz wracam
		ST_DO_NOT_DISTURB,
		ST_INVISIBLE,
		ST_OFFLINE,
		ST_SKYPEME,
		ST_LOGGEDOUT
	};

	enum SkypeProtocols
	{
		PROTOCOL_1 = 1,
		/* Not needed any Skype Protocol versions between 1 and 8 */
		PROTOCOL_8 = 8,
	};

		
	virtual void Connect() = 0;
	virtual void Disconnect() = 0;
	virtual bool IsConnected() = 0;

	void SetName(const QString&) {};
	void SetProtocol(SkypeProtocols) {};

	virtual void SetStatusTo(Status st) = 0;
	virtual Status GetStatus() = 0;

};


#endif // SKYPEPROTOCOL_H
