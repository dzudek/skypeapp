/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#pragma once
#include "SkypeProtocol.h"

#import "Skype4COM.dll" 

class SkypeCOMProtocol :
	public SkypeProtocol
{
public:
	SkypeCOMProtocol(void);
	~SkypeCOMProtocol(void);

	void Connect();
	void Disconnect();
	bool IsConnected();

	void SetStatusTo(Status st);
	Status GetStatus();

private:
	SKYPE4COMLib::ISkypePtr _pSkype;

}; //class SkypeCOMProtocol


