/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#pragma once

#include "ASystemStartup.h"

class WindowsSystemStartup : public ASystemStartup
{
	Q_OBJECT

public:
	bool isSet();

public slots:
	void toggle(bool set = false);
	
};
