/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#pragma once

#include "LocalServer.h"

#if QT_VERSION >= 0x050000
#include <QtWidgets/QApplication>
#else
#include <QtGui/QApplication>
#endif

#include <QLocalSocket>

class SingleApplication : public QApplication
{
	Q_OBJECT
public:
	explicit SingleApplication(int argc, char *argv[]);
	~SingleApplication();

	bool shouldContinue() const { return _shouldContinue; };

signals:
	void showUp();

private slots:
	void onShowUp();

private:
	QLocalSocket *_socket;
	LocalServer *_server;
	bool _shouldContinue;
	
};
