/*
*	Authors:
*	Pawel Drzycimski <pdrzycimski@gmail.com>
*	Karol Judek <jkarol@interia.eu>
*/

#include "mainapplication.h"
#include "ui_mainapplication.h"

#include <QDateTime>

#include "SkypeManager.h"
#ifdef Q_OS_WIN
#include "WindowsSystemStartup.h"
#else
#include "LinuxSystemStartup.h"
#endif
#include "gca/gcaManager.h"
#include "gca/gcaFreeBusy.h"

#include <QIcon>
#include <QAction>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QDebug>
#include <QMessageBox>
#include <QTimer>
#include <QCloseEvent>
#include <QSignalMapper>
#include <QSettings>
#include <QThread>

MainApplication::MainApplication(QWidget *parent) : 
	QMainWindow(parent), 
	ui(new Ui::MainApplication), 
	firstCheck(true)
{
	ui->setupUi(this);

	QSettings settings;

	//setFixedSize(width(), height());
	resize(settings.value("size", sizeHint()).toSize());
	setWindowFlags((windowFlags() | Qt::CustomizeWindowHint) & ~Qt::WindowMaximizeButtonHint);
	
	_checkBoxSignalMapper = new QSignalMapper();

	ui->googleCheckBox->setChecked(settings.value("google/autoconnect", false).toBool());
	connect(ui->googleCheckBox, SIGNAL(clicked()), _checkBoxSignalMapper, SLOT(map()));
	_checkBoxSignalMapper->setMapping(ui->googleCheckBox, "google");

	ui->skypeCheckBox->setChecked(settings.value("skype/autoconnect", false).toBool());
	connect(ui->skypeCheckBox, SIGNAL(clicked()), _checkBoxSignalMapper, SLOT(map()));
	_checkBoxSignalMapper->setMapping(ui->skypeCheckBox, "skype");

	ui->skypeFreeCheckBox->setChecked(settings.value("skype/laststatus", true).toBool());
	connect(ui->skypeFreeCheckBox, SIGNAL(clicked()), _checkBoxSignalMapper, SLOT(map()));
	_checkBoxSignalMapper->setMapping(ui->skypeFreeCheckBox, "skypeFree");
	
	ui->skypeAdvancedCheckBox->setChecked(settings.value("skype/rules", false).toBool());
	connect(ui->skypeAdvancedCheckBox, SIGNAL(clicked()), _checkBoxSignalMapper, SLOT(map()));
	_checkBoxSignalMapper->setMapping(ui->skypeAdvancedCheckBox, "skypeAdvanced");

	connect(_checkBoxSignalMapper, SIGNAL(mapped(const QString &)), this, SLOT(onCheckBoxMapped(const QString &)));

	_comboBoxSignalMapper = new QSignalMapper();

	ui->skypeBusyComboBox->setCurrentIndex(settings.value("skype/statusbusy", 2).toInt()); //default to DND
	connect(ui->skypeBusyComboBox, SIGNAL(currentIndexChanged(int)), _comboBoxSignalMapper, SLOT(map()));
	_comboBoxSignalMapper->setMapping(ui->skypeBusyComboBox, "Busy");

	ui->skypeFreeComboBox->setCurrentIndex(settings.value("skype/statusfree", 0).toInt()); //default to Online
	connect(ui->skypeFreeComboBox, SIGNAL(currentIndexChanged(int)), _comboBoxSignalMapper, SLOT(map()));
	_comboBoxSignalMapper->setMapping(ui->skypeFreeComboBox, "Free");

	connect(_comboBoxSignalMapper, SIGNAL(mapped(const QString &)), this, SLOT(onComboBoxMapped(const QString &)));

#ifdef Q_OS_WIN
	_startup = new WindowsSystemStartup();
#else
	_startup = new LinuxSystemStartup();
#endif
	ui->systemCheckBox->setChecked(_startup->isSet());
	connect(ui->systemCheckBox, SIGNAL(toggled(bool)), _startup, SLOT(toggle(bool)));

	_skypeManager = new SkypeManager();
	connect(ui->skypeButton, SIGNAL(pressed()), _skypeManager, SLOT(ConnectToSkype()));
	connect(_skypeManager, SIGNAL(ChangeStatusError(const QString&)),this,SLOT(OnSkypeError(const QString&)));
	connect(_skypeManager, SIGNAL(ConnectionError(const QString&)),this,SLOT(OnSkypeError(const QString&)));
	connect(_skypeManager, SIGNAL(Connected()),this,SLOT(OnSkypeConnected()));
	connect(_skypeManager, SIGNAL(Disconnected()),this,SLOT(OnSkypeDisconnected()));
	connect(_skypeManager, SIGNAL(ConnectionStatus(const QString&)),this,SLOT(OnSkypeStatus(const QString&)));

	QThread *managerThread = new QThread();
	connect(_skypeManager, SIGNAL(destroyed()), managerThread, SLOT(quit()));
	connect(managerThread, SIGNAL(finished()), managerThread, SLOT(deleteLater()));
	_skypeManager->moveToThread(managerThread);
	managerThread->start();

	_gcaManager = new gcaManager();
	connect(ui->googleButton, SIGNAL(pressed()), _gcaManager, SLOT(connect()));
	connect(_gcaManager, SIGNAL(gcaError(const QString &)), this, SLOT(OnGoogleApiError(const QString &)));
	connect(_gcaManager, SIGNAL(ready()), this, SLOT(onGoogleApiReady()));
	connect(_gcaManager, SIGNAL(connectionChanged(bool)), this, SLOT(onGoogleApiConnectionChanged(const bool &)));

	ui->googleTimeEdit->setTime(settings.value("google/interval", "01:00:00").toTime());
	connect(ui->googleTimeEdit, SIGNAL(timeChanged(const QTime &)), this, SLOT(onGoogleTimeEditTimeChanged(const QTime &)));
	connect(ui->googleTimeEdit, SIGNAL(timeChanged(const QTime &)), _gcaManager, SLOT(intervalChanged()));
	
	_timer = new QTimer(this);
	_timer->setInterval(60000); // 1-minute interval
	//_timer->setInterval(1000); // 1-second interval
	_timer->setTimerType(Qt::VeryCoarseTimer); // "Very coarse timers only keep full second accuracy" - don't need more
	QObject::connect(_timer, SIGNAL(timeout()), this, SLOT(onTimerTimeout()));
	//NOTE: The timer has not started yet, waiting for events list to be ready

	CreateTrayIcon();
}
	
MainApplication::~MainApplication()
{
	qDebug() << "Stopping application";

	DisposeActions();

	if(_timer)
	{
		_timer->stop();
		delete _timer;
		_timer = 0;
	}

	if(ui)
	{
		delete ui;
		ui = 0;
	}

	if(_gcaManager)
	{
		delete _gcaManager;
		_gcaManager = 0;
	}

	if(_skypeManager)
	{
		delete _skypeManager;
		_skypeManager = 0;
	}

	if(_trayIcon)
	{
		delete _trayIcon;
		_trayIcon = 0;
	}

	if(_trayIconMenu)
	{
		delete _trayIconMenu;
		_trayIconMenu = 0;
	}
}

#pragma region "MainApplication public slots"
void MainApplication::onShowUp()
{
	hide();
	showNormal();
	raise();
	setFocus();
}
#pragma endregion

#pragma region "MainApplication protected functions"
void MainApplication::changeEvent(QEvent *e)
{
	switch(e->type())
	{
	case QEvent::WindowStateChange:
		if(isMinimized()) {
			hide();
			e->accept();
			break;
		}
	default:
		QMainWindow::changeEvent(e);
	}
}

void MainApplication::closeEvent(QCloseEvent *e)
{
	hide();
	e->ignore();
}

void MainApplication::resizeEvent(QResizeEvent * evnt)
{
	QSettings().setValue("size", size());

	QWidget::resizeEvent(evnt);
}
#pragma endregion

#pragma region "MainApplication private slots"
void MainApplication::OnShowHide(QSystemTrayIcon::ActivationReason reason)
{
	if(reason && reason != QSystemTrayIcon::DoubleClick){
		return;
	}

	SwitchShowHide();
}

void MainApplication::SwitchShowHide()
{
	if(isVisible())
	{
		hide();
	}
	else
	{
		showNormal();
		raise();
		setFocus();
	}
}

void MainApplication::OnSkypeConnected()
{
	//lastSkypeStatus = _skypeManager->getStatus();
	
	if(gcaFreeBusy::getInstance()->isReady() && !_timer->isActive())
	{
		onTimerTimeout();
		_timer->start();
	}

	//GUI
	disconnect(_skypeConnectAction,SIGNAL(triggered()),_skypeManager,SLOT(ConnectToSkype()));
	disconnect(ui->skypeButton, SIGNAL(pressed()), _skypeManager, SLOT(ConnectToSkype()));
#ifdef __gnu_linux__
	connect(_skypeConnectAction,SIGNAL(triggered()),_skypeManager,SLOT(DisconnectFromSkype()));
	connect(ui->skypeButton, SIGNAL(pressed()), _skypeManager, SLOT(DisconnectFromSkype()));
	ui->skypeButton->setEnabled(true);
#else
	_skypeConnectAction->setDisabled(true);
	ui->skypeButton->setDisabled(true);
#endif

	_skypeConnectAction->setText("Skype: Disconnect");
	ui->skypeLabel->setText("Connected");
	ui->skypeButton->setText("Disconnect");
	//QMessageBox::information(this,"Skype Connected","Skype connection action successed.");
}

void MainApplication::OnSkypeDisconnected()
{
	if(_timer->isActive())
	{
		_timer->stop();
		firstCheck = true;
	}

	// GUI
	connect(_skypeConnectAction,SIGNAL(triggered()),_skypeManager,SLOT(ConnectToSkype()));
	disconnect(_skypeConnectAction,SIGNAL(triggered()),_skypeManager,SLOT(DisconnectFromSkype()));
	
	connect(ui->skypeButton, SIGNAL(pressed()), _skypeManager, SLOT(ConnectToSkype()));
	disconnect(ui->skypeButton, SIGNAL(pressed()), _skypeManager, SLOT(DisconnectFromSkype()));
	
	_skypeConnectAction->setText("Skype: Connect");
	ui->skypeLabel->setText("Disconnected");
	ui->skypeButton->setText("Connect");
	ui->skypeButton->setEnabled(true);
}

void MainApplication::OnSkypeError(const QString &description)
{
	ui->skypeLabel->setText(QString("Error: %1").arg(description));
	//QMessageBox::critical(this,"Skype Error",description);
}

void MainApplication::OnSkypeStatus(const QString &description)
{
	ui->skypeLabel->setText(description);
}

void MainApplication::onGoogleApiConnectionChanged(const bool &connected)
{
	if(connected)
	{
		onGoogleApiConnected();
	}
	else
	{
		onGoogleApiDisconnected();
	}
}

void MainApplication::onGoogleApiConnected()
{
	//lastGoogleBusyStatus = gcaFreeBusy::getInstance()->isBusy();

	// GUI
	disconnect(ui->googleButton, SIGNAL(pressed()), _gcaManager, SLOT(connect()));
	disconnect(_googleCalendarConnect, SIGNAL(triggered()), _gcaManager, SLOT(connect()));

	connect(ui->googleButton, SIGNAL(pressed()), _gcaManager, SLOT(disconnect()));
	connect(_googleCalendarConnect, SIGNAL(triggered()), _gcaManager, SLOT(disconnect()));

	_googleCalendarConnect->setText("Google: Disconnect");
	ui->googleLabel->setText("Connected");
	ui->googleButton->setText("Disconnect");
	ui->googleTimeEdit->setEnabled(true);
	ui->googleTimeLabel->setEnabled(true);
}

void MainApplication::onGoogleApiDisconnected()
{
	if(_timer->isActive())
	{
		_timer->stop();
		firstCheck = true;
	}

	// GUI
	disconnect(ui->googleButton, SIGNAL(pressed()), _gcaManager, SLOT(disconnect()));
	disconnect(_googleCalendarConnect, SIGNAL(triggered()), _gcaManager, SLOT(disconnect()));

	connect(ui->googleButton, SIGNAL(pressed()), _gcaManager, SLOT(connect()));
	connect(_googleCalendarConnect, SIGNAL(triggered()), _gcaManager, SLOT(connect()));

	_googleCalendarConnect->setText("Google: Connect");
	ui->googleLabel->setText("Disconnected");
	ui->googleButton->setText("Connect");
	ui->googleTimeEdit->setDisabled(true);
	ui->googleTimeLabel->setDisabled(true);
}

void MainApplication::onGoogleApiReady()
{
	if(_skypeManager->isConnected() && !_timer->isActive())
	{
		onTimerTimeout();
		_timer->start();
	}
}

void MainApplication::OnGoogleApiError(const QString &desc)
{
	if(_timer->isActive())
	{
		_timer->stop();
		firstCheck = true;
	}
	
	ui->googleLabel->setText(QString("Error: %1").arg(desc));
	//QMessageBox::critical(this, "Google API Error", desc);
}

inline void MainApplication::onTimerTimeout()
{
	updateSkypeStatus(false);
}

void MainApplication::updateSkypeStatus(const bool &forceUpdate /* = true*/)
{
	if(!_skypeManager->isConnected() || !_gcaManager->isConnected())
	{
		qWarning() << "Skipping status update" << _skypeManager->isConnected() << _gcaManager->isConnected();
		return;
	}

	qDebug() << "Updating status" << QDateTime::currentDateTimeUtc().toString(Qt::ISODate) << forceUpdate;

	bool currentGoogleBusyStatus = gcaFreeBusy::getInstance()->isBusy();

	SkypeProtocol::Status currentSkypeStatus = _skypeManager->getStatus();

	if(firstCheck || forceUpdate || currentGoogleBusyStatus != lastGoogleBusyStatus)
	{
		if(firstCheck)
		{
			firstCheck = false;
			lastSkypeStatus = currentSkypeStatus;
		}

		QSettings settings;

		SkypeProtocol::Status busyStatus = (SkypeProtocol::Status)settings.value("skype/statusbusy", 2).toInt();
		SkypeProtocol::Status freeStatus = (SkypeProtocol::Status)settings.value("skype/statusfree", 0).toInt();

		if(!currentGoogleBusyStatus && lastSkypeStatus == busyStatus)
		{
			lastSkypeStatus = freeStatus;
		}

		if(currentGoogleBusyStatus && currentSkypeStatus != busyStatus)
		{
			lastSkypeStatus = currentSkypeStatus;
		}

		if(settings.value("skype/laststatus", true).toBool())
		{
			_skypeManager->ChangeStatusTo(currentGoogleBusyStatus ? busyStatus : lastSkypeStatus);
		}
		else
		{
			_skypeManager->ChangeStatusTo(currentGoogleBusyStatus ? busyStatus : freeStatus);
		}
	}

	lastGoogleBusyStatus = currentGoogleBusyStatus;
}

void MainApplication::onCheckBoxMapped(const QString &text)
{
	qDebug() << "Checkbox changed" << text;

	QString key = text+"/autoconnect";

	//FIXME ugly...
	if(text == "skypeFree") {
		key = "skype/laststatus";
	}
	else if(text == "skypeAdvanced") {
		key ="skype/rules";
	}

	QSettings().setValue(key, centralWidget()->findChild<QCheckBox *>(text+"CheckBox")->isChecked());
}

void MainApplication::onComboBoxMapped(const QString &text)
{
	qDebug() << "ComboBox changed" << text;

	QSettings().setValue("skype/status"+text.toLower(), centralWidget()->findChild<QComboBox *>("skype"+text+"ComboBox")->currentIndex());

	QTimer::singleShot(1000 , this, SLOT(updateSkypeStatus()));
}

void MainApplication::onGoogleTimeEditTimeChanged(const QTime &time)
{
	qDebug() << "Google inverval changed to" << time.toString();
	
	QSettings().setValue("google/interval", ui->googleTimeEdit->time().toString());
}
#pragma endregion

#pragma region "MainApplication private functions"
void MainApplication::CreateTrayIcon()
{
	_trayIcon = new QSystemTrayIcon(this);
	_trayIcon->setIcon(QIcon(":/images/tray_icon.png"));

	connect(_trayIcon,SIGNAL(activated(QSystemTrayIcon::ActivationReason)),this,SLOT(OnShowHide(QSystemTrayIcon::ActivationReason)));

	_trayExitAction = new QAction("Exit",_trayIcon);
	connect(_trayExitAction,SIGNAL(triggered()),qApp,SLOT(quit()));

	_hideAction = new QAction("Options",_trayIcon);
	connect(_hideAction,SIGNAL(triggered()),this,SLOT(SwitchShowHide()));

	//__skype actions________________________
	_skypeConnectAction = new QAction("Skype: Connect",_trayIcon);
	connect(_skypeConnectAction,SIGNAL(triggered()),_skypeManager,SLOT(ConnectToSkype()));

	//__google calendar actions_______________
	_googleCalendarConnect = new QAction("Google: Connect",_trayIcon);
	connect(_googleCalendarConnect,SIGNAL(triggered()), _gcaManager, SLOT(connect()));

	_trayIconMenu = new QMenu(this);
	_trayIconMenu->addAction(_googleCalendarConnect);
	_trayIconMenu->addAction(_skypeConnectAction);
	_trayIconMenu->addSeparator();
	_trayIconMenu->addAction(_hideAction);
	_trayIconMenu->addSeparator();
	_trayIconMenu->addAction(_trayExitAction);

	//_trayIconMenu->setDefaultAction(_hideAction); // just to looks nice

	_trayIcon->setContextMenu(_trayIconMenu);
	_trayIcon->show();
}

void MainApplication::DisposeActions()
{
	DisposeAction(_hideAction);
	DisposeAction(_trayExitAction);

	DisposeAction(_skypeConnectAction);

	DisposeAction(_googleCalendarConnect);
}

void MainApplication::DisposeAction(QAction *action)
{
	if(action)
	{
		delete action;
		action = 0;
	}
}
#pragma endregion
