/*
*	Authors:
*	Pawel Drzycimski <pdrzycimski@gmail.com>
*	Karol Judek <jkarol@interia.eu>
*/

#include "SingleApplication.h"
#include "mainapplication.h"

#ifndef WIN32
#include <QtDBus/QtDBus>
#endif

#include <QObject>
#include <QSettings>
#include <QDateTime>

void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
	QByteArray localMsg = msg.toLocal8Bit();

	if(type < QSettings().value("debug/level", QtCriticalMsg).toInt())
	{
		return;
	}

	FILE *f;
#ifdef Q_OS_WIN
	QString logfile = getenv("appdata");
	logfile += QString("/%1/%2.log").arg(QCoreApplication::organizationName(), QCoreApplication::applicationName());
#endif
	if(fopen_s(&f, QSettings().value("debug/file", logfile).toByteArray().constData(), "a"/*, ccs=UTF-8"*/) != 0)
	{
		return;
	}

	switch (type)
	{
	case QtDebugMsg:
		fprintf_s(f, "%s\tDebug:\t%s (%s:%u, %s)\n", QDateTime::currentDateTime().toString(Qt::ISODate).toLocal8Bit().constData(), localMsg.constData(), context.file, context.line, context.function);
		break;
	case QtWarningMsg:
		fprintf_s(f, "%s\tWarning:\t%s (%s:%u, %s)\n", QDateTime::currentDateTime().toString(Qt::ISODate).toLocal8Bit().constData(), localMsg.constData(), context.file, context.line, context.function);
		break;
	case QtCriticalMsg:
		fprintf_s(f, "%s\tCritical:\t%s (%s:%u, %s)\n", QDateTime::currentDateTime().toString(Qt::ISODate).toLocal8Bit().constData(), localMsg.constData(), context.file, context.line, context.function);
		break;
	case QtFatalMsg:
		fprintf_s(f, "%s\tFatal:\t%s (%s:%u, %s)\n", QDateTime::currentDateTime().toString(Qt::ISODate).toLocal8Bit().constData() , localMsg.constData(), context.file, context.line, context.function);
		abort();
	}

	fclose(f);
}

int main(int argc, char *argv[])
{
	QSettings::setDefaultFormat(QSettings::IniFormat);
	QCoreApplication::setOrganizationName("SkypeApp");
	QCoreApplication::setApplicationName("SkypeApp");

	qInstallMessageHandler(myMessageOutput);

	qDebug() << "Starting application";

	SingleApplication a(argc, argv);
	a.setApplicationDisplayName("Google Calendar Skype plug-in");
	a.setQuitOnLastWindowClosed(false);

	if(!a.shouldContinue())
	{
		qWarning() << "Application already running";
		return 0;
	}

	a.setWindowIcon(QIcon(":/images/window_icon.png"));

	MainApplication w;

	QObject::connect(&a, SIGNAL(showUp()), &w, SLOT(onShowUp()));

	return a.exec();
}
