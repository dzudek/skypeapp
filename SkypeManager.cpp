/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#include "SkypeManager.h"

#include <QThread>
#include <QSettings>
#include <QTimer>
#include <QDebug>

SkypeManager::SkypeManager(QObject *parent /* = 0 */) :
	QObject(parent)
{
#ifndef Q_OS_WIN
	_protocol = new SkypeIPCProtocol(new SkypeDBUSStream());
#else
	_protocol = new SkypeCOMProtocol();
#endif
	
	if(QSettings().value("skype/autoconnect", false).toBool())
	{
		QTimer::singleShot(500, this, SLOT(ConnectToSkype()));
	}
}

void SkypeManager::ConnectToSkype()
{
	qDebug() << "Connecting to Skype";
	emit ConnectionStatus("Connecting");

	try{
		_protocol->Connect();
#ifndef Q_OS_WIN
		_protocol->SetName("Google Calendar Skype plug-in");
		_protocol->SetProtocol(SkypeProtocol::PROTOCOL_1);
#endif
	}
	catch(SkypeException &e)
	{
		DisconnectFromSkype();
		emit ConnectionError(e.msg);
		return;
	}

	if(_protocol->IsConnected())
	{
		qDebug() << "Connected to Skype";
		emit Connected();
		return;
	}

	qWarning() << "Not connected to Skype";
	
	if(QSettings().value("skype/autoconnect", false).toBool())
	{
		qWarning() << "Retrying connection in 5 seconds";
		emit ConnectionStatus("Cannot connect to Skype. Retrying...");
		QTimer::singleShot(5000, this, SLOT(ConnectToSkype()));
	}
}

void SkypeManager::DisconnectFromSkype()
{
	qDebug() << "Disconnecting from Skype";
	emit ConnectionStatus("Disconnecting");

	_protocol->Disconnect();

	if(!_protocol->IsConnected())
	{
		emit Disconnected();
	}
}

void SkypeManager::ChangeStatusTo(SkypeProtocol::Status st)
{
	try{
		_protocol->SetStatusTo(st);
	}
	catch(SkypeException &e)
	{
		emit ChangeStatusError(e.msg);
	}
}

bool SkypeManager::isConnected() const
{
	return _protocol->IsConnected();
}

SkypeProtocol::Status SkypeManager::getStatus() const
{
	if(isConnected())
	{
		return _protocol->GetStatus();
	}

	return SkypeProtocol::ST_NA;
}