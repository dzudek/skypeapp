/*
*	Authors:
*	Karol Judek <jkarol@interia.eu>
*/

#pragma once

#include <QObject>

class ASystemStartup : public QObject
{
	Q_OBJECT

public:
	virtual bool isSet() = 0;

public slots:
	virtual void toggle(bool set = false) = 0;

protected:
	
};
